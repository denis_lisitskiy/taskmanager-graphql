// Core
import { loader } from 'graphql.macro';
import { useMutation } from '@apollo/react-hooks';

// Queries
const mutationUpdateTasks = loader('./gql/mutationUpdateTask.graphql');

export const useTaskUpdater = () => {
    const [ _update] = useMutation(mutationUpdateTasks);

    const updateTask = async (id, task, refetch) => {
        try{
            await _update({
                variables:{
                    id,
                    task
                }
            });
            refetch();
        }catch (error){
            console.error(error);
        }
    };

    return {
        updateTask
    }
}