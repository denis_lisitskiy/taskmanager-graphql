// Core
import { loader } from 'graphql.macro';
import { useMutation } from '@apollo/react-hooks';

// Queries
const mutationAddTask = loader('./gql/mutationAddTask.graphql');

export const useTaskCreator = () => {
    const [ _create, {error}] = useMutation(mutationAddTask);

    const createTask = async (task, refetch) => {
        try{
            await _create({variables:{task}});
            refetch();
        }catch (error){
            console.error(error);
        }
    };

    return {
        createTask,
        error
    }
}