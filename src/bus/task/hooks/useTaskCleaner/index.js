// Core
import { loader } from 'graphql.macro';
import { useMutation } from '@apollo/react-hooks';

// Queries
const mutationRemoveTask = loader('./gql/mutationRemoveTask.graphql');

export const useTaskCleaner = () => {
    const [ _removeTask] = useMutation(mutationRemoveTask);

    const removeTask = async (id, refetch) => {
       try{
           await _removeTask({
               variables: {
                   id
               }
           });
           refetch();
       }
       catch (error){
           console.log(error);
       }

    };

    return {
        removeTask
    }
}