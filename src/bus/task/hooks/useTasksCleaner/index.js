// Core
import { loader } from 'graphql.macro';
import { useMutation } from '@apollo/react-hooks';

// Queries
const mutationRemoveAllTasks = loader('./gql/mutationRemoveAllTasks.graphql');

export const useTasksCleaner = () => {
    const [ _cleanTasks] = useMutation(mutationRemoveAllTasks);

    const removeAllTasks = async () => {
        try{
           await _cleanTasks();
        }catch (error){
            console.error(error);
        }
    };

    return {
        removeAllTasks
    }
}